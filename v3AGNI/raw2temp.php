#!/usr/local/bin/php
<?php
//------------- set necessary paramters -------------------------------------

if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    //WINDOWS user: set path to tools
    $convert='"C:\util\ImageMagick\convert.exe"';
    $exiftool='"C:\util\exiftool\exiftool.exe"';
    //set font variable as needed (Mac/Win) for color scale
    $font='-font c:\windows\Fonts\arialbd.ttf';
    $escape='';

} else {
    //Unix/Mac: set path to tools here 
    $convert='/usr/bin/convert';
    $exiftool='/usr/local/bin/exiftool';
    //set font variable as needed (Mac/Win) for color scale
    $font='-font /Library/Fonts/Arial\ Bold.ttf';
    $escape='\\';
}

//--------------------------------------------------------------------------

$shortopts  = "";
$shortopts .= "i:";  // Required value
$shortopts .= "o:";  
$shortopts .= "h";  

$longopts  = array(
    "resize:",     // Required value
    "rmin:",     
    "rmax:",    
    "pal:",
    "tref:",
	"tatm:",
	"dist:",
	"hum:",
    "emis:",
    "pip::",       // opt. value
    "clut",        // No value
    "scale", 
	"stretch", 	
    "msx",
    "shade",
    "help",       
);
$options = getopt($shortopts, $longopts);
//--------------------------------------------------------------------------
$flirimg="\"".$options['i']."\"";
//get Exif values (syntax for Unix and Windows)
eval('$exif='.shell_exec($exiftool.' -php -flir:all -q '.$flirimg));

// save Flir values for Plancks Law and Atmospheric Transmission for better reading in short variables
$R1=$exif[0]['PlanckR1'];
$R2=$exif[0]['PlanckR2'];
$B= $exif[0]['PlanckB'];
$O= $exif[0]['PlanckO'];
$F= $exif[0]['PlanckF'];
$A1= $exif[0]['AtmosphericTransAlpha1'];
$A2= $exif[0]['AtmosphericTransAlpha2'];
$B1= $exif[0]['AtmosphericTransBeta1'];
$B2= $exif[0]['AtmosphericTransBeta2'];
$X= $exif[0]['AtmosphericTransX'];
$Humidity = $exif[0]['RelativeHumidity'];
$Temp_atm = $exif[0]['AtmosphericTemperature'];
$Distance = $exif[0]['ObjectDistance'];
$Temp_ref = $exif[0]['ReflectedApparentTemperature'];
$Emissivity = $exif[0]['Emissivity'];

print('Plancks values: '.$R1.' '.$R2.' '.$B.' '.$O.' '.$F."\n");
print('Atmosph values: '.$A1.' '.$A2.' '.$B1.' '.$B2.' '.$X."\n\n");

// get displayed temp range in RAW values
$RAWmax=$exif[0]['RawValueMedian']+$exif[0]['RawValueRange']/2;
$RAWmin=$RAWmax-$exif[0]['RawValueRange'];

printf("RAW Temp Range FLIR setting: %d %d\n",$RAWmin,$RAWmax);

// calc atm transmission
$H2o = ($Humidity/100) * exp(1.5587 + 6.939e-2 * $Temp_atm - 2.7816e-4 * pow($Temp_atm,2) + 6.8455e-7 * pow($Temp_atm,3));
$Tau = $X * exp(-sqrt($Distance) * ($A1 + $B1 * sqrt($H2o))) + (1-$X) * exp(-sqrt($Distance) * ($A2 + $B2 * sqrt($H2o)));
print('Atmosph : '.$Temp_atm.'C '.$Humidity.'% '.$Distance."m \nTau=".$Tau."\n");
print("Reflected Apparent Temperature: ".$Temp_ref." degree Celsius\nEmissivity: ".$Emissivity."\n");

// calc amount of radiance from athmosphere 
$RAWatm=$R1/($R2*(exp($B/($Temp_atm+273.15))-$F))-$O;
// calc amount of radiance of reflected objects ( Emissivity < 1 )
$RAWrefl=$R1/($R2*(exp($B/($Temp_ref+273.15))-$F))-$O;
printf("RAW athmosphere: %d / RAW reflected %d\n\n",$RAWatm,$RAWrefl); 

//open file

$ifile = fopen("ir.csv","r");
while($c = fgetcsv($ifile)) $list[] = $c;
fclose($ifile);

for ($row = 0; $row < 120; $row++) {
  for ($col = 0; $col < 120; $col++) {
	  $tmp = ($list[$row][$col]-(1-$Tau)* $RAWatm-(1-$Emissivity)*$Tau*$RAWrefl)/$Emissivity/$Tau;
    $degC[$row][$col] = $B/log($R1/($R2*($tmp+$O))+$F)-273.15;
 
  }
}

$ofile = fopen("irC.csv","w");

foreach ($degC as $fields) {
    fputcsv($ofile, $fields);
}

fclose($ofile);

?>

