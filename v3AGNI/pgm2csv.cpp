#include <iostream> // cout, cerr
#include <fstream> // ifstream
#include <sstream> // stringstream
using namespace std;

int main(int argc, char *argv[]) {
	if (argc !=3){
		cout<<"Usage: pgm2csv infile.pgm outfile.txt"<<endl;
		}
		
  string ifile = argv[1]; //ir image
  string ofile = argv[2]; // out filename
  
  int row = 0, col = 0, numrows = 0, numcols = 0, mval=0;
  ifstream infile((ifile).c_str());
  stringstream ss;
  string inputLine = "";

  // First line : version
  getline(infile,inputLine);
  if(inputLine.compare("P2") != 0) cerr << "Version error" << endl;
  else cout << "Version : " << inputLine << endl;

  // Continue with a stringstream
  ss << infile.rdbuf();
  // Second line : size
  ss >> numcols >> numrows;
  cout << numcols << " columns and " << numrows << " rows" << endl;
  // Third line
  ss>>mval;
  cout <<mval<<endl;
  
  int array[numrows][numcols];

  // Following lines : data
  for(row = 0; row < numrows; ++row)
    for (col = 0; col < numcols; ++col) ss >> array[row][col];
		

  // write file
	ofstream outfile((ofile).c_str()); // create file

			for (int row = 0; row < numrows;++row){
				for(int col = 0; col < numcols; ++col){
					outfile << array[row][col]<<",";
				}
			outfile << endl;
			}
    outfile.close();
}

//Source:
//http://stackoverflow.com/questions/8126815/how-to-read-in-data-from-a-pgm-file-in-c
//  Thanks to BenC
