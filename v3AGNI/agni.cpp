#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main ( int argc, char *argv[] )
{
  if ( argc != 3 ) // argc should be 2 for correct execution
    // We print argv[0] assuming it is the program name
    cout<<"usage: "<< argv[0] <<" <ir_image>.jpg <out_name>\n";
  else {
  
  string ifile = argv[1]; //ir image
  string ofile = argv[2]; // out filename
  
  system(("php IRtoC.php -i"+ifile+" -o ir.jpg").c_str());  // extract sensor values save to .pgm
  
  system(("./pgm2csv ir.pgm ir.txt ")); // convert .pgm to CSV structure (ext = .txt)
  
  system("sed 's/,$//' ir.txt > ir.csv"); //strip eol comma
  
  system(("./graphcut ir.png")); // interactive segmentation (Grabcut)
  
  system("./rmROI"); // remove ROI and resize before processing
  
  system(("./pgm2csv rmROI.pgm rmROI.txt ")); // convert .pgm to CSV structure (ext = .txt)

  system("sed 's/,$//' rmROI.txt > rmROI.csv"); //strip eol comma
  
  system(("php raw2temp.php -i "+ifile).c_str()); // convert sensor values to deg C
  
  system(("./mapper")); //output temperatures of segmented region by masking
  
  system(("sed 's/,$//' result.txt > "+ofile+".csv").c_str());
  
  // GNUplot script launcher if GNUplot is installed (optional). Un-comment to use.
  //system(("gnuplot -e \"filename='"+ofile+".csv'\" 3dPlot").c_str()); 
   
  //cleanup
  
  //remove tmp files (comment for debugging)
  system("rm gradient.png raw.png palette.png gc.png ir.png *.o rmROI.txt ir.txt result.txt");
  
  //remove data files (comment to preserve, but will be over-written) (comment for debugging)
  system("rm rmROI.pgm ir.pgm ir.jpg irC.csv rmROI.csv ir.csv");
  
  //un-comment to remove out_name.txt
  
  //system(("rm " +ofile+".csv").c_str());
  
  }
  
  return 0;
}

