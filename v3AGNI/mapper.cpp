#include <fstream>
#include <sstream>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int dim_x = 120;
int dim_y = 120;

float overlay[120][120];
 
typedef vector <double> record_t;
typedef vector <record_t> data_t;
 
//-----------------------------------------------------------------------------

istream& operator >> ( istream& ins, record_t& record ){
	
  record.clear();
  string line;
  getline( ins, line );
  stringstream ss( line );
  string field;
  
  while (getline( ss, field, ',' )){

    stringstream fs( field );
    double f = 0.0;  // (default value is 0.0)
    fs >> f;
    record.push_back( f );
    }

  return ins;
  }

//-----------------------------------------------------------------------------

istream& operator >> ( istream& ins, data_t& data ){
  
  data.clear();

  record_t record;
  
  while (ins >> record){
    data.push_back( record );
    }

  return ins;  
  }

//-----------------------------------------------------------------------------
int main()
{
   data_t data;

  ifstream threshed("rmROI.csv");
  threshed >> data;
  
  // Complain if something went wrong.
  if (!threshed.eof()){
    cout << "FooeyROI!\n";
    return 1;
    }

  threshed.close();
  
  data_t irData;

  ifstream thermal("irC.csv");
  thermal >> irData;
  
  // Complain if something went wrong.
  if (!thermal.eof()){
    cout << "Fooey!\n";
    return 1;
    }
  thermal.close();
  
// make binary
    for (int row = 0; row < dim_x; row++){
		for(int col = 0; col < dim_y; col++){
			if((int) data[row][col] == 255){
				 data[row][col] = 1;
				continue;
			}
			else{
				data[row][col] = 0;
			}
		}
	}

// mutiply binary(i,j)*irData(i,j)
	for (int row = 0; row < dim_x;row++){
		for(int col = 0; col < dim_y; col++){
			overlay[row][col] = (irData[row][col]*data[row][col]);
		}
	}
				
// write file
	ofstream outfile("result.txt"); // create file

			for (int row = 0; row < dim_x;row++){
				for(int col = 0; col < dim_y; col++){
					if(col < dim_y){
					outfile << overlay[row][col]<<",";
					}
					else if(col == dim_y){
					outfile << overlay[row][col]<<"\n";
					}
				}
			outfile << endl;
			}
    outfile.close();
    
    return 0;
}
