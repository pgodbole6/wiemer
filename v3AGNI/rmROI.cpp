#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>
#include <assert.h>     /* assert */

using namespace std;
using namespace cv;

Mat rmROI(const Mat& src)
{
    assert(src.type() == CV_8UC3);

    Mat noGreen;
    //syntax: inRange(src, Scalar(lowBlue, lowGreen, lowRed), Scalar(highBlue, highGreen, highRed), redColorOnly);
    cv::inRange(src, Scalar(0, 0, 0), Scalar(0, 255, 0), noGreen);
    
    return noGreen;
}

int main(int argc, char** argv)
{
	Mat out;
	Mat out_inv;
	
    Mat input = imread("gc.png");
    
    Mat noGreen = rmROI(input); //filter
    
    resize(noGreen,out,cvSize(120,120),INTER_CUBIC);//resize image
    bitwise_not(out, out_inv); //invert
    
    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PXM_BINARY);
    compression_params.push_back(0);
    
	imwrite("rmROI.pgm",out_inv,compression_params);
	
    return 0;
}

/*Source:
 * http://stackoverflow.com/questions/9018906/detect-rgb-color-interval-with-opencv-and-c*/
